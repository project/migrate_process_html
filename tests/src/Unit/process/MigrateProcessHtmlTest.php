<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_process_html\Unit\process;

use Drupal\migrate_process_html\Plugin\migrate\process\MigrateProcessHtml;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Tests the migrate_process_html process plugin.
 *
 * @group migrate_process_html
 * @coversDefaultClass \Drupal\migrate_process_html\Plugin\migrate\process\MigrateProcessHtml
 */
final class MigrateProcessHtmlTest extends MigrateProcessTestCase {

  /**
   * A logger prophecy object.
   *
   * Using ::setTestLogger(), this prophecy will be configured and injected into
   * the container. Using $this->logger->function(args)->shouldHaveBeenCalled()
   * you can assert that the logger was called.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * Mock of http_client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $mockHttp;

  /**
   * The data fetcher plugin definition.
   */
  private array $pluginDefinition = [
    'id' => 'migrate_process_html',
    'title' => 'Migrate Process HTML',
    'jsredirect' => TRUE,
  ];

  /**
   * Method to toggle the jsredirect state.
   */
  public function toggleJavascriptRedirect() {
    if ($this->pluginDefinition['jsredirect'] === TRUE) {
      $this->pluginDefinition['jsredirect'] = FALSE;
    }
    else {
      $this->pluginDefinition['jsredirect'] = TRUE;
    }
  }

  /**
   * Test MigrateProcessHtml with jsredirect redirect support set to TRUE.
   */
  public function testMigrateProcessHtmlWithJavascriptRedirect() {

    $this->toggleJavascriptRedirect();

    $configuration = [];
    $value = 'https://news.google.com/rss/articles/CBMiVWh0dHBzOi8vd3d3LmxvbmRvbi1maXJlLmdvdi51ay9pbmNpZGVudHMvMjAyMy9qYW51YXJ5L21haXNvbmV0dGUtZmlyZS1zdHJlYXRoYW0taGlsbC_SAQA?oc=5';

    $google = file_get_contents(__DIR__ . '/../../../fixtures/files/google.html');
    $lfb = file_get_contents(__DIR__ . '/../../../fixtures/files/lfb.html');
    $mock = new MockHandler([
      new Response(200, [], $google),
      new Response(200, [], $lfb),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $document = (new MigrateProcessHtml($configuration, 'migrate_process_html', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($lfb, $document, $message = "actual value is not equal to expected");

    // Reset.
    $this->toggleJavascriptRedirect();
  }

  /**
   * Test MigrateProcessHtml with JavaScript redirect support set to FALSE.
   */
  public function testMigrateProcessHtmlWithoutJavascriptRedirect() {

    $configuration = [];
    $value = 'https://news.google.com/rss/articles/CBMiVWh0dHBzOi8vd3d3LmxvbmRvbi1maXJlLmdvdi51ay9pbmNpZGVudHMvMjAyMy9qYW51YXJ5L21haXNvbmV0dGUtZmlyZS1zdHJlYXRoYW0taGlsbC_SAQA?oc=5';

    $lfb = file_get_contents(__DIR__ . '/../../../fixtures/files/lfb.html');
    $mock = new MockHandler([
      new Response(200, [], $lfb),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $document = (new MigrateProcessHtml($configuration, 'migrate_process_html', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($lfb, $document, $message = "actual value is not equal to expected");

  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    $this->row = $this->createMock('Drupal\migrate\Row');

    $this->migrateExecutable = $this->createMock('Drupal\migrate\MigrateExecutable');

    $mockHttp = $this
      ->getMockBuilder('GuzzleHttp\Client')
      ->disableOriginalConstructor()
      ->addMethods(['getBody'])
      ->onlyMethods(['get'])
      ->getMock();
    $this->mockHttp = $mockHttp;

    parent::setUp();

  }

}
